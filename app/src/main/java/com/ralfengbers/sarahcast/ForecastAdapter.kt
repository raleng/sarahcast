package com.ralfengbers.sarahcast

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.forecast_card.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs


class ForecastAdapter(private var forecastData: MutableList<JSONArray>, private var sunriseTides: JSONObject) :
    RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {

    class ForecastViewHolder(cv: View) : RecyclerView.ViewHolder(cv) {
        val cardView: CardView = cv.findViewById(R.id.card_view)
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastAdapter.ForecastViewHolder {
        val cv = LayoutInflater.from(parent.context)
            .inflate(R.layout.forecast_card, parent, false)
        return ForecastViewHolder(cv)
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        // get the data for the day at position
        var sunrise: String? = null
        var lowTide: String? = null

        val data = forecastData[position]

        val firstEntry = data[0] as JSONObject
        val sunriseTidesDate = MainActivity.getSunriseDate(firstEntry.getLong("localTimestamp"))

        if (sunriseTides.has(sunriseTidesDate)) {
            val sunriseTidesJSON = sunriseTides[sunriseTidesDate] as JSONObject

            sunrise = sunriseTidesJSON.getString("sunrise")

            if (sunriseTidesJSON.has("tides")) {
                val tidesJSON = sunriseTidesJSON["tides"] as JSONObject
                lowTide = getTideString(tidesJSON)
            }
        }

        // iterate through the day and find the hour closest to sunrise (either 5, 6 or 7)
        for (i in 0 until data.length()) {

            val entry = data[i] as JSONObject
            val hour = getHour(entry.getLong("localTimestamp"))
            Log.d("TIME", getFullTime(entry.getLong("localTimestamp")))
            when (hour) {
                // show the forecast closest to sunrise
                5, 6, 7 -> {
                    setCardViewData(holder, hour, entry, sunrise, lowTide)
                    Log.d("ONBIND", "%s, %s".format(hour.toString(), entry.toString()))
                }
                else -> {
                    // if the earliest hour is after sunrise, or the last forecast is before sunrise, show that instead
                    if ((hour > 7 && i == 0) || (hour < 5 && data.length() == 1)) {
                        setCardViewData(holder, hour, data[0] as JSONObject, sunrise, lowTide)
                        Log.d("ONBIND", "else %s".format(hour.toString()))
                    }
                }
            }
        }
    }


    override fun getItemCount() = forecastData.size


    fun setData(forecastData: MutableList<JSONArray>, sunriseTides: JSONObject) {
        this.forecastData = forecastData
        this.sunriseTides = sunriseTides
        notifyDataSetChanged()
    }

    /** getHour
     *
     *  return the hour of the current forecast entry as an integer
     */
    private fun getHour(s: Long): Int {
        return try {
            // pattern "H" means hour as integer (0..23)
            SimpleDateFormat("H", Locale.US).format(Date(s * 1000)).toInt()
        } catch (e: Exception) {
            Log.d("getHour", e.toString())
        }
    }

    /** getFullTime
     *
     * return the full time for the forecast for the CardView title
     */
    private fun getFullTime(s: Long): String {
        return try {
            SimpleDateFormat("E, dd.MM.").format(Date(s * 1000))
        } catch (e: Exception) {
            Log.d("getFullTime", e.toString())
            ""
        }
    }

    private fun getTideString(tidesJSON: JSONObject): String {
        val low = tidesJSON.getJSONArray("low")
        val high = tidesJSON.getJSONArray("high")

        val lowHourString = low[0] as String
        val lowHour = lowHourString.split(":")[0].toInt()
        val highHourString = high[0] as String
        val highHour = highHourString.split(":")[0].toInt()


        return if (abs(lowHour - 6) <= abs(highHour -6) ) {
            "Low: %s".format(lowHourString)
        } else {
            "High: %s".format(highHourString)
        }
    }
    /** setCardViewData
     *
     * from each JSONObject extract the info and set the respective views
     *
     * Needs:
     * - localTimestamp
     * - swell.components.{primary,secondary,tertiary}
     */
    private fun setCardViewData(holder: ForecastViewHolder, hour: Int, data: JSONObject, sunrise: String? = null, lowTide: String? = null) {
        try {
            val time = data.getLong("localTimestamp")
            val swell = data.getJSONObject("swell").getJSONObject("components")
            val primarySwell = swell.getJSONObject("primary")

            val swellSizeString = "%s ft".format(primarySwell.getDouble("height").toString())
            val swellPeriodString = "%s sec".format(primarySwell.getInt("period").toString())

            holder.cardView.apply {
                card_title_view.text = getFullTime(time)
                sunrise_view.text = sunrise ?: ""
                tide_view.text = lowTide ?: ""
                swell_size_view.text = swellSizeString
                swell_period_view.text = swellPeriodString
                primary_swell_view.rotation =
                    primarySwell.getString("direction").toFloat()

                // set secondary swell, if it exists; if not, make swell direction indicator invisible
                try {
                    secondary_swell_view.rotation = swell.getJSONObject("secondary").getString("direction").toFloat()
                    secondary_swell_view.visibility = View.VISIBLE
                } catch (e: Exception) {
                    secondary_swell_view.visibility = View.INVISIBLE
                }
                // set tertiary swell, if it exists; if not, make swell direction indicator invisible
                try {
                    tertiary_swell_view.rotation = swell.getJSONObject("tertiary").getString("direction").toFloat()
                    tertiary_swell_view.visibility = View.VISIBLE
                } catch (e: Exception) {
                    tertiary_swell_view.visibility = View.INVISIBLE
                }
            }
        } catch (e: Exception) {
            e.toString()
        }
    }
}