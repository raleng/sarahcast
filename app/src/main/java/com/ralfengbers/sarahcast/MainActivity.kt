package com.ralfengbers.sarahcast

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.Serializable
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var sunzalFile: File
    private lateinit var puntarocaFile: File
    private lateinit var sunriseTidesFile: File

    private val apiKey = BuildConfig.MSW_API_KEY
    private val fieldsUnits = "&units=uk"
    private val url = "http://magicseaweed.com/api/$apiKey/forecast/?spot_id="

    private val sunriseTideApiKey = BuildConfig.SUNRISE_TIDE_API_KEY
    private val sunriseTidesUrl = "https://raleng.pythonanywhere.com/sunrise-tide-api/v1.0"

    private var forecastSunzal = mutableListOf<JSONArray>()
    private var forecastPuntaroca = mutableListOf<JSONArray>()
    private var sunriseTides = JSONObject("{}")

    private lateinit var sunzalFragment: ForecastFragment
    private lateinit var puntarocaFragment: ForecastFragment

    private lateinit var queue: RequestQueue

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("RUN", "on create")
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        // If no forecast is cached, start the fragment_no_forecast
        // otherwise, load the forecast and start the TabLayout
        sunzalFile = File(filesDir, getString(R.string.sunzal_filename)).apply {
            if (exists()) forecastSunzal = reorderArray(JSONArray(readText()))
        }
        puntarocaFile = File(filesDir, getString(R.string.puntaroca_filename)).apply {
            if (exists()) forecastPuntaroca = reorderArray(JSONArray(readText()))
        }

        sunriseTidesFile = File(filesDir, getString(R.string.sunrise_filename)).apply {
            if (exists()) {
                Log.d("File", "sunrise tides files")
                sunriseTides = JSONObject(readText())
            }
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        viewpager.adapter = mSectionsPagerAdapter

        findViewById<TabLayout>(R.id.tab_layout).apply {
            setupWithViewPager(viewpager)
        }
    }


    /** getForecast
     *
     * Use a Volley JSONArray request to get the forecast data
     *
     * On success:
     * - save the response to file
     * - reorder the array to aggregate the days
     * - instantiate the fragments or update data
     */
    fun getForecast(id: Int) {
        // Get the forecast according to the SpotID
        Log.d("RUN", "getForecast")

        // Create a new queue and generate the API URL
        queue = Volley.newRequestQueue(this)
        val requestURL = "$url$id$fieldsUnits" //""%s%s%s".format(url, id, fieldsUnits)

        // Generate the JSONArray Request
        StringRequest(Request.Method.GET, requestURL,
            Response.Listener<String> { response ->
                try {
                    Log.d("RUN", "getForecast: response")
                    JSONArray(response)
                    forecastResponse(id, response)
                } catch (e: JSONException) {
                    Log.d("RUN", "getForecast: json exception")
                    JSONObject(response).getJSONObject("error_response").apply {
                        if (getInt("code") == 119) {
                            Toast.makeText(
                                applicationContext,
                                "Access to Forecast denied.".format(id),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        errorToast(e.toString())
                    }
                }
            },
            Response.ErrorListener { error ->
                when (error) {
                    is ServerError -> Toast.makeText(
                        applicationContext,
                        "Magicseaweed Server Error.",
                        Toast.LENGTH_LONG
                    ).show()
                    is NoConnectionError -> Toast.makeText(
                        applicationContext,
                        "No Internet Connection.",
                        Toast.LENGTH_LONG
                    ).show()
                    else -> Toast.makeText(
                        applicationContext,
                        "Some unknown error occurred.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Log.d("RUN", "getForecast: error listener")

            }
        ).apply { queue.add(this) }
        // add the request to the volley queue
        Log.d("RUN", "getForecast: queue.add")
    }


    /**
     * forecastResponse
     *
     * extracted from getForecast to simplify the try-catch block
     *
     * - writes string response to file
     * - converts and reorders string to day accumulated JSONArray
     * - sets ForecastFragment or updates it
     */
    private fun forecastResponse(id: Int, forecastString: String) {
        Log.d("RUN", "forecastResponse")
        when (id) {
            spotIdSunzal -> {
                // write the response to file for caching
                Log.d("FILE", "writesunzal File")
                writeJSONFile(sunzalFile, forecastString)
                Log.d("FILE", sunzalFile.exists().toString())
                Log.d("FILE", sunzalFile.readText())
                // reorder array to group single days
                forecastSunzal = reorderArray(JSONArray(forecastString))

                if (sunriseTidesNeedUpdate(forecastSunzal, sunriseTides)) {
                    getSunriseTides(id)
                } else {
                    // update the fragments if the initial NoForecastFragment is still in use
                    // ELSE update the fragment data
                    updateSpot(spotIdSunzal)
                }
            }
            spotIdPuntaroca -> {
                // write the response to file for caching
                writeJSONFile(puntarocaFile, forecastString)

                // reorder array to group single days
                forecastPuntaroca = reorderArray(JSONArray(forecastString))

                if (sunriseTidesNeedUpdate(forecastPuntaroca, sunriseTides)) {
                    getSunriseTides(id)
                } else {
                    updateSpot(spotIdPuntaroca)
                }
            }
        }
    }

    /**
    REORDER ARRAY

    The JSONArray from the API response is just listing every three hour forecast. Since the ForecastFragments in the
    overview only shows the forecast for each day that is the closest to sunrise, all forecasts are grouped by day in
    separate JSONArrays.
     */
    private fun reorderArray(forecastData: JSONArray): MutableList<JSONArray> {

        // initialize the return list
        val returnList = mutableListOf<JSONArray>()

        // check for empty forecastData
        // TODO: this necessary? Probably not
        if (forecastData.length() > 0) {
            // initialize the array for the current day and dayBefore
            var currentArray = JSONArray()
            var dayBefore = 0

            // go through the whole data set and check whether the current item has the same
            // date as the one before
            // if they are the same, add to current array, if not start new array
            // append all arrays to the return list
            for (i in 0 until (forecastData.length())) {

                val data = forecastData[i] as JSONObject
                val day = getDay(data.getLong("localTimestamp"))

                //
                when {
                    dayBefore == 0 -> currentArray.put(data)
                    day == dayBefore -> currentArray.put(data)
                    else -> {
                        returnList.add(currentArray)
                        currentArray = JSONArray().apply { put(data) }
                    }
                }
                dayBefore = day
            }
            returnList.add(currentArray)
        }
        return returnList
    }


    /** sunriseTidesNeedUpdate
     *
     * Check if there are sunrise and tides times for the last forecast day
     */
    private fun sunriseTidesNeedUpdate(
        forecast: MutableList<JSONArray>,
        sunriseTides: JSONObject
    ): Boolean {
        val forecastLastDayJSON = forecast.last()[0] as JSONObject
        val forecastLastDay = getSunriseDate(forecastLastDayJSON.getLong("localTimestamp"))

        // check if last forecast day is in sunrise/tides data
        // if YES: check if both sunrise and tides are there
        return if (sunriseTides.has(forecastLastDay)) {
            !(sunriseTides.getJSONObject(forecastLastDay).has("sunrise") &&
                    sunriseTides.getJSONObject(forecastLastDay).has("tides"))
        } else {
            true
        }
    }


    private fun getSunriseTides(id: Int) {
        // Get the forecast according to the SpotID
        Log.d("RUN", "getSunriseTides")

        // Create a new queue and generate the API URL
        queue = Volley.newRequestQueue(this)

        // Generate the JSONArray Request
        object : JsonObjectRequest(
            Method.GET,
            sunriseTidesUrl,
            null,
            Response.Listener<JSONObject> { response ->
                try {
                    sunriseTides = response
                    writeJSONFile(sunriseTidesFile, sunriseTides.toString())
                    updateSpot(id)

                    Log.d("SUNRISE", response.toString())
                } catch (e: JSONException) {
                    updateSpot(id)
                    Log.d("SUNRISE", "error")
                }

            },
            Response.ErrorListener { error ->
                when (error) {
                    is ServerError -> Toast.makeText(
                        applicationContext,
                        "Magicseaweed Server Error.",
                        Toast.LENGTH_LONG
                    ).show()
                    is NoConnectionError -> Toast.makeText(
                        applicationContext,
                        "No Internet Connection.",
                        Toast.LENGTH_LONG
                    ).show()
                    else -> Toast.makeText(
                        applicationContext,
                        "Some unknown error occurred.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                updateSpot(id)
            }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val credentials = "sarahcast:$sunriseTideApiKey"
                val auth = "Basic %s".format(
                    Base64.encodeToString(
                        credentials.toByteArray(),
                        Base64.DEFAULT
                    )
                )
                return hashMapOf(Pair("Authorization", auth))
            }
        }.also {
            Log.d("HEADERS", it.headers.toString())
            queue.add(it)
        }
    }


    private fun updateSpot(id: Int) {
        Log.d("RUN", "updateSpot")
        when (id) {
            spotIdSunzal -> {
                if (sunzalFragment is NoForecastFragment) {
                    Log.d("FRAGMENT", "is NO")
                    mSectionsPagerAdapter?.notifyDataSetChanged()
                } else {
                    Log.d("FRAGMENT", "is update")
                    sunzalFragment.updateFragmentData(forecastSunzal, sunriseTides)
                }
            }

            spotIdPuntaroca -> {
                if (puntarocaFragment is NoForecastFragment) {
                    mSectionsPagerAdapter?.notifyDataSetChanged()
                } else {
                    puntarocaFragment.updateFragmentData(forecastPuntaroca, sunriseTides)
                }
            }
        }
    }


    /** getDay
     *
     * return the day of the month as an integer
     */
    private fun getDay(s: Long): Int {
        return try {
            // pattern "d" means "day of the month as number (1..31)
            SimpleDateFormat("d", Locale.US).format(Date(s * 1000)).toInt()
        } catch (e: Exception) {
            Log.d("getDay", e.toString())
        }
    }


    private fun writeJSONFile(filename: File, forecast: String) {
        filename.printWriter().use { out -> out.print(forecast) }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        // TODO: implement
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // TODO: implement
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    private fun errorToast(errorText: String) {
        Toast.makeText(applicationContext, errorText, Toast.LENGTH_LONG).show()
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     *
     * If a cached (in file) forecast exists, it returns a ForecastFragment
     * If not, it returns a NoForecastFragment
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): ForecastFragment? {
            // getItem is called to instantiate the fragment for the given page.
            // Return a ForecastFragment or a NoForecastFragment, depending on whether
            // a cached (in file) forecast exists
            return when (position) {
                0 -> {
                    Log.d("PAGER", "%s".format(forecastSunzal.toString()))
                    if (forecastSunzal.size != 0) {
                        ForecastFragment.newInstance(spotIdSunzal, forecastSunzal, sunriseTides)
                    } else {
                        NoForecastFragment.newInstance(spotIdSunzal)
                    }.also { sunzalFragment = it }
                }
                1 -> {
                    Log.d("PAGER", "%s".format(forecastPuntaroca.toString()))
                    if (forecastPuntaroca.size != 0) {
                        ForecastFragment.newInstance(
                            spotIdPuntaroca,
                            forecastPuntaroca,
                            sunriseTides
                        )
                    } else {
                        NoForecastFragment.newInstance(spotIdPuntaroca)
                    }.also { puntarocaFragment = it }
                }
                else -> null
            }
        }


        override fun getCount(): Int {
            return 2
        }


        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> getString(R.string.el_sunzal)
                1 -> getString(R.string.punta_roca)
                else -> ""
            }
        }

        // Check if NoForecastFragments exist and remove them
        // (POSITION_NONE tags them to be replaced)
        // This is not totally optimal, since NoForecastFragments will be replaced by new NoForecastFragments, but
        // when all ForecastFragments are initialized, this is not relevant anymore
        override fun getItemPosition(obj: Any): Int {
            return if (obj is NoForecastFragment) {
                Log.d("ADAPTER", "NONE")
                PagerAdapter.POSITION_NONE
            } else {
                Log.d("ADAPTER", "UNCHANGED")
                PagerAdapter.POSITION_UNCHANGED
            }
        }
    }


    companion object {
        const val spotIdSunzal = 440
        const val spotIdPuntaroca = 439

        fun getSunriseDate(s: Long): String {
            return try {
                SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Date(s * 1000))
            } catch (e: Exception) {
                Log.d("getSunriseDate", e.toString())
                ""
            }
        }
    }


    /**
     * Forecast Fragment
     *
     * implements a SwipeRefreshLayout and a RecyclerView
     */
    open class ForecastFragment : Fragment() {

        private lateinit var forecast: MutableList<JSONArray>
        private lateinit var sunriseTides: JSONObject
        private lateinit var viewAdapter: ForecastAdapter

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(R.layout.fragment_forecast, container, false)
            val spotId = arguments?.getInt(ARG_SPOT_ID)

            // remove the forecast from bundle to avoid problems saving and loading a list of JSONArrays
            // it is not needed anyways, since we have the forecast cached in files
            arguments?.apply {
                forecast = this.getSerializable(ARG_FORECAST) as MutableList<JSONArray>
                sunriseTides = JSONObject(this.getString(ARG_SUNRISETIDES))
                remove(ARG_FORECAST)
                remove(ARG_SUNRISETIDES)
            }

            // initialize a viewManager and the recycler view adapter
            viewAdapter = ForecastAdapter(forecast, sunriseTides)

            // apply viewManager and adapter to the recyclerView
            rootView.findViewById<RecyclerView>(R.id.forecast_recycler_view).apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = viewAdapter
                hasFixedSize()
            }

            // set up the swipe refresh
            val swipeContainer =
                rootView.findViewById<SwipeRefreshLayout>(R.id.swipeContainerForecast)

            swipeContainer.setOnRefreshListener {
                spotId?.let { (activity as MainActivity).getForecast(spotId) } ?: Toast.makeText(
                    activity,
                    "Spot ID not found.",
                    Toast.LENGTH_LONG
                ).show()
                swipeContainer.isRefreshing = false
            }

            return rootView
        }


        fun updateFragmentData(forecast: MutableList<JSONArray>, sunriseTides: JSONObject) {
            // update the forecast data and call the adapter function to set the new data
            this.forecast = forecast
            this.sunriseTides = sunriseTides
            viewAdapter.setData(this.forecast, this.sunriseTides)
            Toast.makeText(activity, "Forecast updated".format(id), Toast.LENGTH_SHORT).show()
        }


        companion object {

            private const val ARG_SPOT_ID = "spotId"
            private const val ARG_FORECAST = "forecast"
            private const val ARG_SUNRISETIDES = "sunriseTides"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(
                id: Int,
                forecast: MutableList<JSONArray>,
                sunriseTides: JSONObject
            ): ForecastFragment {
                return ForecastFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_SPOT_ID, id)
                        putSerializable(ARG_FORECAST, forecast as Serializable)
                        putString(ARG_SUNRISETIDES, sunriseTides.toString())
                    }
                }
            }
        }
    }


    /**
     * NoForecastFragment
     *
     * A simple fragment to be shown, when the app is first started and no cached forecast is available.
     * When the first forecast is successfully loaded, this is replaced by the FragmentManager with a ForecastFragment
     */
    class NoForecastFragment : ForecastFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {

            val rootView = inflater.inflate(R.layout.fragment_no_forecast, container, false)
            val spotId = arguments?.getInt(ARG_SPOT_ID)

            val swipeContainer =
                rootView.findViewById<SwipeRefreshLayout>(R.id.swipeContainerNoForecast)
            swipeContainer.setOnRefreshListener {
                spotId?.let { (activity as MainActivity).getForecast(it) }
                swipeContainer.isRefreshing = false
            }
            return rootView
        }

        companion object {
            private const val ARG_SPOT_ID = "spotId"

            fun newInstance(id: Int): NoForecastFragment {
                return NoForecastFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_SPOT_ID, id)
                    }
                }
            }
        }
    }
}
